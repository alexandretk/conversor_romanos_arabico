package test;

import static org.junit.Assert.*;
import conversor.*;

import org.junit.Before;
import org.junit.Test;

public class NumerosRomanosTest {
	
	private Conversor conversor;

	@Before
	public void setup() throws Exception {
		conversor = new Conversor();
	}
	
	@Test
	public void testRomanosParaArabicos() {
		
		assertEquals(1, conversor.romanosParaArabico("I"));
		assertEquals(2, conversor.romanosParaArabico("II"));
		assertEquals(3, conversor.romanosParaArabico("III"));
		assertEquals(11, conversor.romanosParaArabico("XI"));
		assertEquals(8, conversor.romanosParaArabico("IIX"));
		assertEquals(9, conversor.romanosParaArabico("IX"));		
		assertEquals(10, conversor.romanosParaArabico("X"));
		assertEquals(50, conversor.romanosParaArabico("L"));
		assertEquals(70, conversor.romanosParaArabico("LXX"));
		assertEquals(80, conversor.romanosParaArabico("LXXX"));
		assertEquals(39, conversor.romanosParaArabico("IXL"));
		assertEquals(41, conversor.romanosParaArabico("XLI"));
		assertEquals(100, conversor.romanosParaArabico("C"));
		assertEquals(160, conversor.romanosParaArabico("CLX"));
		assertEquals(500, conversor.romanosParaArabico("D"));
		assertEquals(1000, conversor.romanosParaArabico("M"));
		assertEquals(998, conversor.romanosParaArabico("IIM"));
		assertEquals(1106, conversor.romanosParaArabico("MCVXI"));
		assertEquals(1710, conversor.romanosParaArabico("MDCCX"));
		assertEquals(1800, conversor.romanosParaArabico("MDCCC"));
		assertEquals(389, conversor.romanosParaArabico("IXCD"));
		assertEquals(1389, conversor.romanosParaArabico("MIXCD"));
		assertEquals(1727, conversor.romanosParaArabico("MDCCXXVII"));
	}
}


